package mlc.bdd;

import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import mlc.pages.GeoApiActions;

public class GeoApiStepDefinitions {

	
	 @Steps
	    private GeoApiActions geoApiActions;

	    @Given("connect to the API")
	    public void givenIHaveAnAddress() {
	        geoApiActions.givenIHaveAnAPI();
	    }
	    
	    @Then("verify api response")
	    public void verifyApiResponse() {
	        geoApiActions.verifyApiResponse();
	    }

	    @Then("verify api status code")
	    public void thenDataShouldBeReceived() {
	    	geoApiActions.verifyApiResponseStatus();
	    }
}
