package mlc.bdd;

import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import mlc.meta.MLCEndUserSteps;
import cucumber.api.java.en.And;

public class LifeViewStepDefinition  {

    @Steps
    MLCEndUserSteps steps;

    @Given("The user is on the MLC home page")
    public void givenTheUserIsOnTheMLCHomePage() {
        steps.is_the_home_page();
    }

    @When("Search for Lifeview")
    public void whenTheUserSearch() {
        steps.enters("Lifeview");
    }

    @Then("Open the Lifeview link")
    public void thenTheyShouldSeeALinkContainingTheWords() {
        steps.clickLifeViewLink();
    }
    
    @And("Request for Demo")
   
    public void andRequestDemo()  {
          steps.shouldRequestDemo();
      }
    

}
