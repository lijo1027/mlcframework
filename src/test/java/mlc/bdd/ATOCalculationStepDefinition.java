package mlc.bdd;

import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import mlc.meta.ATOEndUserSteps;

public class ATOCalculationStepDefinition  {

    @Steps
    ATOEndUserSteps steps;

    @Given("^The user is on the ATO Tax calculator page$")
    public void givenTheUserIsOnTheATOCalculatorPage() {
        steps.is_the_home_page();
    }

    @Then("^Select the year by index '(.*)'$")
    public void selectYear(int index) {
        steps.selectYear(index);
    }
    
    @And("^Enter the income '(.*)'$")
        public void andEnterIncom(String income) {
          steps.enterIncome(income);
      }

    @And("^Select residency status$")
    public void selectResidency() {
        steps.selectResidency();
    }
    @And("^Submit$")
    public void submit() {
        steps.submit();
    }
    
    @Then("^Validate tax calculated$")
    public void validateTaxCalculated() 
    {
    	steps.validateTaxCalculated();
    }
    
}
