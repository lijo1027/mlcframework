package mlc;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

/**
 * This class contains the generic utility-level functions for GUIs.

 */
public class WDBase {
	
	/*
	 * Variable declarations 
	 */
	public static WebDriver wd = null;
	
	/**
	 * Set-up Webdriver.
	 */
	public static void startWD(String browser, String url, 
			String pageLoadTimeout, String implicitWait){
		
				
		//1) Initializing browser
		if (browser.equalsIgnoreCase("firefox")){
			System.setProperty("webdriver.gecko.driver", "././configs/geckodriver.exe");
			FirefoxProfile fp = new FirefoxProfile();
			fp.setPreference("network.proxy.type", 0);
			wd = new FirefoxDriver();
			
		}else if(browser.equalsIgnoreCase("ie")){
			System.setProperty("webdriver.ie.driver", "././configs/");
			wd = new InternetExplorerDriver();
		} else if (browser.equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver", "././configs/chromedriver.exe");
			wd = new ChromeDriver();
		}else
			fail("Unsupport browser '" + browser + "' specified.");
		
		//2) Setting timeouts
		wd.manage().timeouts().pageLoadTimeout(Long.parseLong(pageLoadTimeout), TimeUnit.SECONDS);
		wd.manage().timeouts().implicitlyWait(Long.parseLong(implicitWait), TimeUnit.SECONDS);
		
		//3) Opening url
		wd.get(url);
	}
	
	
	/**
	 * Tear down Webdriver.
	 */
	public static void stopWD(){
		if (wd !=null){
			try {
				wd.quit();
			} catch (WebDriverException e) {
				
				e.printStackTrace();
			}
			wd = null;
		}
		
	}
	
	
	/**
	 * Clicks on the required page objects.
	 * @param by The criteria for locating the page object 
	 * 
	 */
	public static void click(By by){
		wd.findElement(by).click();
	}
	
	/**
	 * Enter text in the required text box page object.
	 * @param by The criteria for locating the page object
	 * @param text Text to be typed or entered
	 */
	public static void type(By by, String text){
		WebElement textBox = wd.findElement(by);
		textBox.clear();
		textBox.sendKeys(text);
		textBox = null;
	}
	
	/**
	 * Gets the text of the page object.
	 * @param by The criteria for locating the page object 
	 */
	public static String getText(By by){
		return wd.findElement(by).getText();
	}
	
	/**
	 * Select by required text from the selection page object.
	 * @param by The criteria for locating the page object
	 * @param text Text to be selected
	 */
	public static void selectByVisibleText(By by, String text){
		WebElement we = wd.findElement(by);
		Select selectionObj = new Select(we);
	    selectionObj.selectByVisibleText(text);
	}
	
	/**
	 * Check whether element exists on GUI.
	 * @param by The criteria for locating the page object
	 * @return <b>true</b>: If element exists, else returns <b>false</b>.
	 */
	public static boolean isElementExists(By by){
		return wd.findElements(by).size() !=0;
	}
	
	/**
	 *
	 * Check whether element is visible on GUI.
	 * @param by The criteria for locating the page object
	 * @return <b>true</b>: If element is visible, else returns <b>false</b>.
	 */
	public static boolean isElementVisible(By by){
		if (isElementExists(by)){
			return wd.findElement(by).isDisplayed();
		}
		return false;
	}
	
	/**
	 * Press enter on the required control.
	 */
	public static void pressEnter(By by){
		wd.findElement(by).sendKeys(Keys.ENTER);
	}
	
	/**
	 * Clicks on required control using JavaScript.
	 */
	public static void jsClick(By by){
		JavascriptExecutor e = (JavascriptExecutor)wd;
		e.executeScript("arguments[0].click();", wd.findElement(by));
		e=null;
	}
	
	/**
	 * Enters data using javascript.
	 */
	public static void jsType(By by, String text) {
		
		JavascriptExecutor e = (JavascriptExecutor)wd;
//		e.executeScript("arguments[0].setAttribute('value', '" + 
//				text + "');", wd.findElement(by));
		e.executeScript("arguments[0].setAttribute('value', arguments[1])", wd.findElement(by), text);
		e=null;
	}
	
	/**
	 * Select the checkbox, if not already selected.
	 */
	public static void check(By by){
		WebElement we = wd.findElement(by);
		if (!we.isSelected())
			we.click();
		we = null;
	}
	
	/**
	 * Deselect the checkbox, if not already deselected.
	 */
	public static void unCheck(By by){
		WebElement we = wd.findElement(by);
		if (we.isSelected())
			we.click();
		we = null;
	}
	

	
	public static void waitForVisibility(By by){
		WebDriverWait wait = new WebDriverWait(wd, 400);
		wait.until(
		        ExpectedConditions.visibilityOfElementLocated(by));
		
	}
	
	public static void waitForAvailability(By by){
		WebDriverWait wait = new WebDriverWait(wd, 60);
		wait.until(
		        ExpectedConditions.elementToBeClickable(by));
		
	}
	
	
	public static String getAttribute(By by, String attributeName) {
		return wd.findElement(by).getAttribute(attributeName);
	}
	
	public static boolean isEnabled(By by){
		boolean isEnabled = false;
		
		if (isElementVisible(by))
			return wd.findElement(by).isEnabled();
		
		return isEnabled;
	}
	
	public static void pressEscapeKey(By by) {
		wd.findElement(by).sendKeys(Keys.ESCAPE);
	}
	
	public static void pressTabKey(By by) {
		wd.findElement(by).sendKeys(Keys.TAB);
	}
	
	public static boolean isSelected(By by) {
		return wd.findElement(by).isSelected();
	}
}
