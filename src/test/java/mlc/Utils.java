package mlc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Contains generic utility functions.
 
 */
public class Utils {
	
	
	public static int counter = 1;
	// class variable
	private static final String lexiconAlphabets = "abcdefghijklmnopqrstuvxyz";//"abcdefghijklmnopqrstuvxyz12345674890";
	private static final String lexiconDigits = "12345674890";

	private static final java.util.Random rand = new java.util.Random();

	// consider using a Map<String,Boolean> to say whether the identifier is being used or not 
	private static final Set<String> identifiers = new HashSet<String>();
	
	/**
	 * Returns the current date in the required format.
	 */
	public static String getCurrentDate(String format)
	{
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/**
	 * Sleep for the required duration.
	 * @param millisecs 
	 */
	public static void sleep(long millisecs){
		try {
			Thread.sleep(millisecs);
		} catch (InterruptedException e) {	}
	}
	
	public static String addOrSubstractDate(int yearMonthDays, int noOfDays) {
		Calendar cal = Calendar.getInstance();
		cal.add(yearMonthDays, noOfDays);
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);
	}
	
	public static String getRandomString() {
		
	    StringBuilder builder = new StringBuilder();
	    while(builder.toString().length() == 0) {
	        int length = rand.nextInt(5)+5;
	        for(int i = 0; i < length; i++) {
	            builder.append(lexiconAlphabets.charAt(rand.nextInt(lexiconAlphabets.length())));
	        }
	        if(identifiers.contains(builder.toString())) {
	            builder = new StringBuilder();
	        }
	    }
	    return builder.toString();
		
	}
	
	public static String getRandomNumber() {
		
	    StringBuilder builder = new StringBuilder();
	    while(builder.toString().length() == 0) {
	        int length = rand.nextInt(5)+5;
	        for(int i = 0; i < length; i++) {
	            builder.append(lexiconDigits.charAt(rand.nextInt(lexiconDigits.length())));
	        }
	        if(identifiers.contains(builder.toString())) {
	            builder = new StringBuilder();
	        }
	    }
	    return builder.toString();
		
	}
	
}
