package mlc.meta;

import mlc.pages.LifeViewHome;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;;

public class MLCEndUserSteps {
	public static final String BREADCRUMB="Home/Partnering with us/Superannuation funds/Lifeview";	

	public static final String NAME = "First";
	public static final String COMPANY = "Company";
	public static final String PHONE = "0469476465";
	public static final String EMAIL = "mail@ct.com";
	public static final String REQUEST_DET = "Test";


	LifeViewHome lifeViewPage;

	@Step
	public void is_the_home_page() {
		lifeViewPage.open();  }

	@Step
	public void enters(String keyword) {
		lifeViewPage.enter_keywords(keyword);
	}

	@Step
	public void clickLifeViewLink() {
		
		lifeViewPage.clickLifeViewLink();
		//assertThat(lifeViewPage.getBreadcrumb(), equalTo(BREADCRUMB));
	}

    @Step
   
    public void shouldRequestDemo() {
    	lifeViewPage.clickDemoRequest();
    	lifeViewPage.enterPersonDetailTab(lifeViewPage.createPersonDetailData(NAME, COMPANY,EMAIL ,PHONE, REQUEST_DET));
    }



}