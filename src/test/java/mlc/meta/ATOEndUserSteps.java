package mlc.meta;

import mlc.pages.ATOCalculation;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;;

public class ATOEndUserSteps {

	ATOCalculation atoCal;

	@Step
	public void is_the_home_page() {
		atoCal.open();  }

	
    @Step
	public void selectYear(int index) {
    	atoCal.selectYear(index);
		
	}

    @Step
	public void enterIncome(String income) {
		atoCal.enterIncome(income);
		
	}

    @Step
	public void selectResidency() {
    	atoCal.selectResident();
		
	}

    @Step
	public void submit() {
    	atoCal.submit();
		
	}

    @Step
	public void validateTaxCalculated() {
		assertTrue("Tax calculated for the income is as per norms",atoCal.validateTax());
		
	}



}