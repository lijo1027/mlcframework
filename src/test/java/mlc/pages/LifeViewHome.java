package mlc.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import static mlc.WDBase.*;
import static mlc.Utils.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import org.openqa.selenium.WebElement;

@DefaultUrl("https://www.mlcinsurance.com.au/")
public class LifeViewHome extends PageObject  {

	public static WebDriver wd = null;
	public static final String SEARCH_IMG = "//*[@id='nav-onscreen']/button";
	public static final String BREADCRUMB="//div[@class='breadcrumb']";		
	public static final String TXT_PREFIX="//*[@id='wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields";
	public static final String TXT_PER_NAME=TXT_PREFIX+"_0__Value']";
	public static final String TXT_PER_COMPANY=TXT_PREFIX+"_1__Value']";
	public static final String TXT_PER_EMAIL=TXT_PREFIX+"_2__Value']";
	public static final String TXT_PER_PHONE=TXT_PREFIX+"_3__Value']";
	public static final String TXT_REQ_DET=TXT_PREFIX+"_6__Value']";
	public static final String RD_TIME=TXT_PREFIX+"_5__Value']";

	@FindBy(xpath=TXT_PER_NAME)
	WebElementFacade firstName;

	@FindBy(xpath=TXT_PER_COMPANY)
	WebElementFacade company;


	@FindBy(xpath=TXT_PER_EMAIL)
	WebElementFacade email;

	@FindBy(xpath=TXT_PER_PHONE)
	WebElementFacade phone;

	@FindBy(xpath=TXT_REQ_DET)
	WebElementFacade reqDet;

	@FindBy(xpath=RD_TIME)
	WebElementFacade rdTime;


	@FindBy(name="global-search")
	WebElementFacade searchTerms;

	@FindBy(partialLinkText="LifeView")
	WebElementFacade lookupButton;

	@FindBy(xpath="//*[@id='main']/div[2]/p[2]/a/span")
	WebElementFacade demReq;

	@FindBy(xpath="//button[@data-popover-id='global-search']")
	WebElementFacade searchButton;


	@FindBy(id="q")
	WebElementFacade searchBox;

	@FindBy(className="breadcrumbs")
	WebElementFacade breadCrumb;

	public void enter_keywords(String keyword) {

		clickOn(searchButton);
		sleep(1000);
		searchBox.type(keyword);
		sleep(5000);
	}

	public String getBreadcrumb(){
	
		return breadCrumb.getText();

	}

	public void clickLifeViewLink() {
		sleep(5000);
		clickOn(lookupButton);
		sleep(5000);
	}
	public void clickDemoRequest() {
		clickOn(demReq);
	}

	/**
	 * Enum for maintaining 'Person Details' for Demo

	 */
	public enum PersonDetailsFields{
		NAME,
		COMPANY,
		EMAIL,
		PHONE,
		REQUEST_DET
		;
	}

	public static HashMap<PersonDetailsFields, Object> createPersonDetailData(String name, String company, 
			String email, String phone,String reqDetail){
		HashMap<PersonDetailsFields, Object> d = new HashMap<PersonDetailsFields, Object>();
		d.put(PersonDetailsFields.NAME, name);
		d.put(PersonDetailsFields.COMPANY, company);
		d.put(PersonDetailsFields.EMAIL, email);
		d.put(PersonDetailsFields.PHONE, phone);
		d.put(PersonDetailsFields.REQUEST_DET, reqDetail);

		return d;
	}

	public  void enterPersonDetailTab(HashMap<PersonDetailsFields, Object> personData){
		System.out.println("Map size ****"+personData.size());
		for (Entry<PersonDetailsFields, Object> d : personData.entrySet())
		{  System.out.println("d.getKey()*********"+d.getKey());
		System.out.println("d.size*********"+d.getKey());
		if (d.getKey().equals(PersonDetailsFields.COMPANY))
		{	sleep(2000);
		System.out.println("d.getKey()*********"+d.getKey()+d.getValue().toString());
		element(company).waitUntilVisible();
		clickOn(company);
		company.type(d.getValue().toString());
		}

		else if (d.getKey().equals(PersonDetailsFields.EMAIL))		
		{  	sleep(1000);
		element(email).waitUntilVisible();
		clickOn(email);
		email.type(d.getValue().toString());
		}
		else if (d.getKey().equals(PersonDetailsFields.PHONE))
		{	
			element(phone).waitUntilVisible();
			phone.type(d.getValue().toString());
		}
		else if (d.getKey().equals(PersonDetailsFields.REQUEST_DET))
		{  
			element(reqDet).waitUntilVisible();
			clickOn(reqDet);
			reqDet.type(d.getValue().toString());
		}
		else if (d.getKey().equals(PersonDetailsFields.NAME))
		{  
			element(firstName).waitUntilVisible();
			clickOn(firstName);
			firstName.type(d.getValue().toString());
		}
		}

		clickOn(rdTime);

	}

}