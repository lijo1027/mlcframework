package mlc.pages;

import java.util.HashMap;
import java.util.Map;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;



public class GeoApiActions {
	
	// https://jsonplaceholder.typicode.com/guide.html
	
	
	@Step
    public void givenIPostApi(){
        
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("userId", 1);
        jsonAsMap.put("title", "Testing");
        jsonAsMap.put("completed", "Testing api");

        SerenityRest.given().contentType("application/json")
                .content(jsonAsMap).log().body()
                .baseUri("https://jsonplaceholder.typicode.com")
                .basePath("todos/1")
        .when().post();
    }
	
    @Step
    public void givenIHaveAnAPI(){
        SerenityRest.given().contentType("application/json")
                .when().get("https://jsonplaceholder.typicode.com/todos/1");
    }
    
    

    @Step
    public void verifyApiResponse(){
    	int id = SerenityRest.then().extract().body().jsonPath().get("userId");
    	System.out.println("water water " + id);
    }

    
    @Step
    public void verifyApiResponseStatus(){
        SerenityRest.then().log().all().
                statusCode(200);
    }



}
