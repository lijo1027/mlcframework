package mlc.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import static mlc.WDBase.*;
import static mlc.Utils.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import static org.junit.Assert.*;

import org.openqa.selenium.WebElement;

@DefaultUrl(" https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=STC&anchor=STC#STC/questions")
public class ATOCalculation extends PageObject  {

	boolean taxValue=false;	
	int incomeVar= 0;

	

	@FindBy(id="ddl-financialYear")
	private WebElementFacade selectYear;

	@FindBy(id="texttaxIncomeAmt")
	private WebElementFacade incomeTax;

	@FindBy(id="vrb-resident-span-0")
	private WebElementFacade resident;

	@FindBy(id="bnav-n1-btn4")
	private WebElementFacade submit;
	
	@FindBy(id="bnav-n1-btn4")
	private WebElementFacade restart;
	
	@FindBy(xpath="//*[@id='applicationHost']/div/div/div/div[2]/div[2]/div/div[1]/div/div/div/p/span")
	private WebElementFacade tax;

	public void selectResident() {
		resident.click();
	}


	public void selectYear(int index) {
		selectYear.selectByIndex(index);
	}


	public void enterIncome(String income) {
		incomeVar=Integer.parseInt(income);
		clickOn(incomeTax);
		sleep(1000);
		incomeTax.type(income);
		sleep(5000);
		
		
	}


	public void submit() {
		submit.click();
		
	}
	


	public boolean validateTax() {
if(incomeVar==50000 && tax.getText().equals("$7,797.00"))
  {
	System.out.println("Tax value"+tax.getText());
	return taxValue=true;
	 
		}
		else if(incomeVar==60000 && tax.getText().equals("$11,047.00")){
			System.out.println("Tax value"+tax.getText());
			return taxValue=true;
						
		}
		else if(incomeVar==20000 && tax.getText().equals("$342.00")){
			
			System.out.println("Tax value"+tax.getText());
			return taxValue=true;
		}
     return taxValue;
	}
}