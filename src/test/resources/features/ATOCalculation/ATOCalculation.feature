Feature: In ATO Website calculate the income tax

@test_ATO
Scenario Outline: User is on ATO Tax Calculator

    Given The user is on the ATO Tax calculator page 
    Then Select the year by index '<index>'
    And Enter the income '<income>' 
    And Select residency status
    And Submit
    Then Validate tax calculated
  
    Examples: 
   |index | income|
   |1     | 50000 |
   |2     | 60000 |
   |3     | 20000 |
    