Feature: Search for Lifeview link and request a Demo

@test_LifeView
Scenario: User is on the Homepage and searches for Lifeview

    Given The user is on the MLC home page 
    When Search for Lifeview 
    Then Open the Lifeview link
    And Request for Demo
   